﻿namespace KanzoBus.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}