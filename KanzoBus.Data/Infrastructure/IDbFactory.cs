﻿using System;

namespace KanzoBus.Data.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        KanzoBusEntities Init();
    }
}