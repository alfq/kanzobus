﻿namespace KanzoBus.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        private KanzoBusEntities _dbContext;

        public KanzoBusEntities Init()
        {
            return _dbContext ?? (_dbContext = new KanzoBusEntities());
        }

        protected override void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}