namespace KanzoBus.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AfterRollback : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Customers", "BusId", "dbo.Buses");
            DropForeignKey("dbo.Drivers", "BusId", "dbo.Buses");
            DropForeignKey("dbo.Users", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.Users", "DriverId", "dbo.Drivers");
            DropIndex("dbo.Customers", new[] { "BusId" });
            DropIndex("dbo.Drivers", new[] { "BusId" });
            DropIndex("dbo.Users", new[] { "DriverId" });
            DropIndex("dbo.Users", new[] { "CustomerId" });
            DropPrimaryKey("dbo.Customers");
            DropPrimaryKey("dbo.Drivers");
            CreateTable(
                "dbo.Bookings",
                c => new
                    {
                        BookingId = c.Int(nullable: false, identity: true),
                        From = c.String(nullable: false, maxLength: 50),
                        To = c.String(nullable: false, maxLength: 50),
                        Date = c.DateTime(nullable: false),
                        Time = c.DateTime(nullable: false),
                        BusType = c.Int(nullable: false),
                        BoadingLocation = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.BookingId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        UserId = c.Int(nullable: false),
                        FirstName = c.String(nullable: false, maxLength: 50),
                        LastName = c.String(nullable: false, maxLength: 50),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                        Bus_BusId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Buses", t => t.Bus_BusId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex")
                .Index(t => t.Bus_BusId);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        TicketId = c.Int(nullable: false, identity: true),
                        BookingId = c.Int(nullable: false),
                        DriverId = c.Int(nullable: false),
                        CustomerId = c.Int(nullable: false),
                        DateTime = c.DateTime(nullable: false),
                        Customer_Id = c.String(maxLength: 128),
                        Driver_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.TicketId)
                .ForeignKey("dbo.Bookings", t => t.BookingId, cascadeDelete: true)
                .ForeignKey("dbo.Customers", t => t.Customer_Id)
                .ForeignKey("dbo.Drivers", t => t.Driver_Id)
                .Index(t => t.BookingId)
                .Index(t => t.Customer_Id)
                .Index(t => t.Driver_Id);
            
            AddColumn("dbo.Customers", "Id", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.Drivers", "Id", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Customers", "CustomerId", c => c.Int(nullable: false));
            AlterColumn("dbo.Drivers", "DriverId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Customers", "Id");
            AddPrimaryKey("dbo.Drivers", "Id");
            CreateIndex("dbo.Customers", "Id");
            CreateIndex("dbo.Drivers", "Id");
            AddForeignKey("dbo.Customers", "Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Drivers", "Id", "dbo.AspNetUsers", "Id");
            DropColumn("dbo.Customers", "FirstName");
            DropColumn("dbo.Customers", "LastName");
            DropColumn("dbo.Customers", "PasswordHash");
            DropColumn("dbo.Customers", "Email");
            DropColumn("dbo.Customers", "Password");
            DropColumn("dbo.Customers", "BusId");
            DropColumn("dbo.Drivers", "FirstName");
            DropColumn("dbo.Drivers", "LastName");
            DropColumn("dbo.Drivers", "PasswordHash");
            DropColumn("dbo.Drivers", "Email");
            DropColumn("dbo.Drivers", "Password");
            DropColumn("dbo.Drivers", "BusId");
            DropTable("dbo.Users");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        DriverId = c.Int(nullable: false),
                        CustomerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            AddColumn("dbo.Drivers", "BusId", c => c.Int(nullable: false));
            AddColumn("dbo.Drivers", "Password", c => c.String(nullable: false));
            AddColumn("dbo.Drivers", "Email", c => c.String(nullable: false, maxLength: 255));
            AddColumn("dbo.Drivers", "PasswordHash", c => c.String());
            AddColumn("dbo.Drivers", "LastName", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.Drivers", "FirstName", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.Customers", "BusId", c => c.Int(nullable: false));
            AddColumn("dbo.Customers", "Password", c => c.String(nullable: false));
            AddColumn("dbo.Customers", "Email", c => c.String(nullable: false, maxLength: 255));
            AddColumn("dbo.Customers", "PasswordHash", c => c.String());
            AddColumn("dbo.Customers", "LastName", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.Customers", "FirstName", c => c.String(nullable: false, maxLength: 50));
            DropForeignKey("dbo.Drivers", "Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Customers", "Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Tickets", "Driver_Id", "dbo.Drivers");
            DropForeignKey("dbo.Tickets", "Customer_Id", "dbo.Customers");
            DropForeignKey("dbo.Tickets", "BookingId", "dbo.Bookings");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetUsers", "Bus_BusId", "dbo.Buses");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Drivers", new[] { "Id" });
            DropIndex("dbo.Customers", new[] { "Id" });
            DropIndex("dbo.Tickets", new[] { "Driver_Id" });
            DropIndex("dbo.Tickets", new[] { "Customer_Id" });
            DropIndex("dbo.Tickets", new[] { "BookingId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", new[] { "Bus_BusId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropPrimaryKey("dbo.Drivers");
            DropPrimaryKey("dbo.Customers");
            AlterColumn("dbo.Drivers", "DriverId", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Customers", "CustomerId", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Drivers", "Id");
            DropColumn("dbo.Customers", "Id");
            DropTable("dbo.Tickets");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Bookings");
            AddPrimaryKey("dbo.Drivers", "DriverId");
            AddPrimaryKey("dbo.Customers", "CustomerId");
            CreateIndex("dbo.Users", "CustomerId");
            CreateIndex("dbo.Users", "DriverId");
            CreateIndex("dbo.Drivers", "BusId");
            CreateIndex("dbo.Customers", "BusId");
            AddForeignKey("dbo.Users", "DriverId", "dbo.Drivers", "DriverId");
            AddForeignKey("dbo.Users", "CustomerId", "dbo.Customers", "CustomerId", cascadeDelete: true);
            AddForeignKey("dbo.Drivers", "BusId", "dbo.Buses", "BusId", cascadeDelete: true);
            AddForeignKey("dbo.Customers", "BusId", "dbo.Buses", "BusId", cascadeDelete: true);
        }
    }
}
