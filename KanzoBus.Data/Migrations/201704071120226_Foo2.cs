namespace KanzoBus.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Foo2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Drivers", "DriverId", c => c.Int(nullable: false, identity: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Drivers", "DriverId", c => c.Int(nullable: false));
        }
    }
}
