namespace KanzoBus.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Buses",
                c => new
                    {
                        BusId = c.Int(nullable: false, identity: true),
                        BusNumber = c.String(nullable: false, maxLength: 15),
                        ServiceType = c.Int(nullable: false),
                        SeatsNumber = c.Int(nullable: false),
                        From = c.String(nullable: false),
                        To = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.BusId);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        CustomerId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 50),
                        LastName = c.String(nullable: false, maxLength: 50),
                        PasswordHash = c.String(),
                        Email = c.String(nullable: false, maxLength: 255),
                        Password = c.String(nullable: false),
                        BusId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CustomerId)
                .ForeignKey("dbo.Buses", t => t.BusId, cascadeDelete: true)
                .Index(t => t.BusId);
            
            CreateTable(
                "dbo.Drivers",
                c => new
                    {
                        DriverId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 50),
                        LastName = c.String(nullable: false, maxLength: 50),
                        PasswordHash = c.String(),
                        Email = c.String(nullable: false, maxLength: 255),
                        Password = c.String(nullable: false),
                        Qualification = c.String(nullable: false, maxLength: 1),
                        BusId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DriverId)
                .ForeignKey("dbo.Buses", t => t.BusId, cascadeDelete: true)
                .Index(t => t.BusId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        DriverId = c.Int(nullable: false),
                        CustomerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Customers", t => t.CustomerId, cascadeDelete: true)
                .ForeignKey("dbo.Drivers", t => t.DriverId)
                .Index(t => t.DriverId)
                .Index(t => t.CustomerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "DriverId", "dbo.Drivers");
            DropForeignKey("dbo.Users", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.Drivers", "BusId", "dbo.Buses");
            DropForeignKey("dbo.Customers", "BusId", "dbo.Buses");
            DropIndex("dbo.Users", new[] { "CustomerId" });
            DropIndex("dbo.Users", new[] { "DriverId" });
            DropIndex("dbo.Drivers", new[] { "BusId" });
            DropIndex("dbo.Customers", new[] { "BusId" });
            DropTable("dbo.Users");
            DropTable("dbo.Drivers");
            DropTable("dbo.Customers");
            DropTable("dbo.Buses");
        }
    }
}
