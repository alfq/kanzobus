﻿using KanzoBus.Data.Infrastructure;
using KanzoBus.Model;

namespace KanzoBus.Data.Repository
{
    public class DriverRepository : RepositoryBase<Driver> , IDriverRepository
    {
        public DriverRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public interface IDriverRepository : IRepository<Driver>
    {
        
    }
}