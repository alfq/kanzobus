﻿using KanzoBus.Data.Infrastructure;
using KanzoBus.Model;

namespace KanzoBus.Data.Repository
{
    public class CustomerRepository : RepositoryBase<Customer> , ICustomerRepository
    {
        public CustomerRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public interface ICustomerRepository : IRepository<Customer> { }
}