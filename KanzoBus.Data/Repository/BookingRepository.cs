﻿using KanzoBus.Data.Infrastructure;
using KanzoBus.Model;

namespace KanzoBus.Data.Repository
{
    public class BookingRepository : RepositoryBase<Booking> , IBookingRepository
    {
        public BookingRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public interface IBookingRepository : IRepository<Booking> { }
}