﻿using KanzoBus.Data.Infrastructure;
using KanzoBus.Model;

namespace KanzoBus.Data.Repository
{
    public class TicketRepository : RepositoryBase<Ticket>
    {
        public TicketRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public interface ITicketRepository : IRepository<Ticket>
    {
        
    }
}