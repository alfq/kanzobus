﻿using KanzoBus.Data.Infrastructure;
using KanzoBus.Model;

namespace KanzoBus.Data.Repository
{
    public class UserRepository : RepositoryBase<User> ,IUserRepository
    {
        public UserRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public interface IUserRepository : IRepository<User>
    {
        
    }
}