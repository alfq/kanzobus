﻿using KanzoBus.Data.Infrastructure;
using KanzoBus.Model;

namespace KanzoBus.Data.Repository
{
    public class BusRepository: RepositoryBase<Bus>, IBusRepository
    {
        public BusRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public interface IBusRepository : IRepository<Bus> { }
}