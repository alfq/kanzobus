﻿using System.Data.Entity.ModelConfiguration;
using KanzoBus.Model;

namespace KanzoBus.Data.Configuration
{
    public class BookingConfiguration : EntityTypeConfiguration<Booking>
    {
        public BookingConfiguration()
        {
            ToTable("Bookings");
            Property(b=>b.From).IsRequired().HasMaxLength(50);
            Property(b=>b.To).IsRequired().HasMaxLength(50);
            Property(b => b.Date).IsRequired();
            Property(b => b.Time).IsRequired();
            Property(b => b.BusType).IsRequired();
            Property(b => b.BoadingLocation).IsRequired();
        }
    }
}