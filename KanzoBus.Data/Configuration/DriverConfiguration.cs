﻿using System.Data.Entity.ModelConfiguration;
using KanzoBus.Model;

namespace KanzoBus.Data.Configuration
{
    public class DriverConfiguration :EntityTypeConfiguration<Driver>
    {
        public DriverConfiguration()
        {
            ToTable("Drivers");
            Property(d => d.FirstName).IsRequired().HasMaxLength(50);
            Property(d => d.LastName).IsRequired().HasMaxLength(50);
            //Property(d => d.Email).IsRequired().HasMaxLength(255);
            //Property(d => d.Password).IsRequired();
            Property(d => d.Qualification).IsRequired().HasMaxLength(1);
        }
    }
}