using System.Data.Entity.ModelConfiguration;
using KanzoBus.Model;

namespace KanzoBus.Data.Configuration
{
    public class BusConfiguration : EntityTypeConfiguration<Bus>
    {
        public BusConfiguration()
        {
            ToTable("Buses");
            Property(b => b.BusNumber).IsRequired().HasMaxLength(15);
            Property(b => b.From).IsRequired();
            Property(b => b.To).IsRequired();
            Property(b => b.SeatsNumber).IsRequired();
            Property(b => b.ServiceType).IsRequired();
        }
    }
}