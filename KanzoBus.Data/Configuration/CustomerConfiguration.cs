using System.Data.Entity.ModelConfiguration;
using KanzoBus.Model;

namespace KanzoBus.Data.Configuration
{
    public class CustomerConfiguration : EntityTypeConfiguration<Customer>
    {
        public CustomerConfiguration()
        {
            ToTable("Customers");
            Property(d => d.FirstName).IsRequired().HasMaxLength(50);
            Property(d => d.LastName).IsRequired().HasMaxLength(50);
            //Property(d => d.Email).IsRequired().HasMaxLength(255);
            //Property(d => d.Password).IsRequired();

        }
    }
}