using System.Data.Entity.ModelConfiguration;
using KanzoBus.Model;

namespace KanzoBus.Data.Configuration
{
    public class TicketConfiguration : EntityTypeConfiguration<Ticket>
    {
        public TicketConfiguration()
        {
            ToTable("Tickets");
            Property(t => t.CustomerId).IsRequired();
            Property(t => t.BookingId).IsRequired();
            Property(t => t.DriverId).IsRequired();
        }
    }
}