﻿using System.Data.Entity;
using KanzoBus.Data.Configuration;
using KanzoBus.Model;
using Microsoft.AspNet.Identity.EntityFramework;

namespace KanzoBus.Data
{
    public class KanzoBusEntities : IdentityDbContext<User>
    {
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Bus> Buses { get; set; }
        //public DbSet<User> Users { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Ticket> Tickets { get; set; }

        public KanzoBusEntities() : base("KanzoBusEntities")
        {        
        }

        public static KanzoBusEntities Create()
        {
            return new KanzoBusEntities();
        }

        public void Commit()
        {
            SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new DriverConfiguration());
            modelBuilder.Configurations.Add(new CustomerConfiguration());
            modelBuilder.Configurations.Add(new BusConfiguration());
            modelBuilder.Configurations.Add(new TicketConfiguration());
            modelBuilder.Configurations.Add(new BookingConfiguration());
            //modelBuilder.Entity<User>()
            //    .HasRequired(x => x.Driver)
            //    .WithMany().WillCascadeOnDelete(false);
            //modelBuilder.Entity<Ticket>()
            //    .HasRequired(x=>x.Driver)
            //    .WithMany().WillCascadeOnDelete(false);
        }
    }
}