using AutoMapper;
using KanzoBus.Model;
using KanzoBus.Web.ViewModels;

namespace KanzoBus.Web.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName => "ViewModelToDomainMappings";

        public ViewModelToDomainMappingProfile()
        {
            CreateMap<DriverViewModel, Driver>();
            CreateMap<BusViewModel,Bus>();
            CreateMap<RegisterViewModel,Customer>();
        }
    }
}