﻿using AutoMapper;
using KanzoBus.Model;
using KanzoBus.Web.ViewModels;

namespace KanzoBus.Web.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        public DomainToViewModelMappingProfile()
        {
            CreateMap<Driver,DriverViewModel>();
            CreateMap<Bus,BusViewModel>();
            CreateMap<Customer,RegisterViewModel>();
        }
    }
}