﻿using KanzoBus.Service;

namespace KanzoBus.Web.ViewModels
{
    public class DriverViewModel
    {
        //public int DriverId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Qualification { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }

    }
}