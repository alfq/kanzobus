﻿using KanzoBus.Model;

namespace KanzoBus.Web.ViewModels
{
    public class BusViewModel
    {
        public int BusId { get; set; }
        public string BusNumber { get; set; }
        public BusType ServiceType { get; set; }
        public int SeatsNumber { get; set; }
        public string From { get; set; }
        public string To { get; set; }
    }
}