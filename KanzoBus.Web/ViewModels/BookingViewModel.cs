﻿using System;
using KanzoBus.Model;

namespace KanzoBus.Web.ViewModels
{
    public class BookingViewModel
    {
        public int Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        public BusType BusService { get; set; }
    }
}