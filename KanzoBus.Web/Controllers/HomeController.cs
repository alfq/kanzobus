﻿using System.Web.Mvc;

namespace KanzoBus.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}