﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using KanzoBus.Model;
using KanzoBus.Service;
using KanzoBus.Web.ViewModels;

namespace KanzoBus.Web.Controllers
{
    public class RegisterController : Controller
    {
        private readonly ICustomerService customerService;

        public RegisterController()
        {
            
        }

        public RegisterController(ICustomerService customerService)
        {
            this.customerService = customerService;
        }

        // GET: Register
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(RegisterViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            var customer = Mapper.Map<RegisterViewModel, Customer>(viewModel);
            customerService.CreateCustomer(customer);
            customerService.SaveCustomer();

            return RedirectToAction("Index", "Home");
        }
    }
}