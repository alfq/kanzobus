﻿using System.Net.Http;
using System.Web.Http;
using KanzoBus.Data;
using KanzoBus.Service;
using KanzoBus.Web.Infrastructure;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace KanzoBus.Web.Controllers
{
    public class BaseApiController : ApiController
    {

        private ApplicationUserManager _appUserManager;

        protected ApplicationUserManager AppUserManager
        {
            get { return _appUserManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
        }

        protected IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

    }
}