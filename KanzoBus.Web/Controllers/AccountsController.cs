﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using AutoMapper;
using KanzoBus.Data.Infrastructure;
using KanzoBus.Data.Repository;
using KanzoBus.Model;
using KanzoBus.Service;
using KanzoBus.Web.ViewModels;
using Microsoft.AspNet.Identity;

namespace KanzoBus.Web.Controllers
{

    [RoutePrefix("api/accounts")]
    public class AccountsController : BaseApiController
    {
        private IUserService userService;

        public AccountsController(IUserService userService)
        {
            this.userService = userService;
        }

        public AccountsController()
        {
            
        }

        [Route("users")]
        public IHttpActionResult GetUsers()
        {
            try
            {
                var users = this.userService.GetUsers().ToList();
                return Ok(users);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        [Route("user/{id:guid}")]
        public async Task<IHttpActionResult> GetUser(string Id)
        {
            var user = await this.AppUserManager.FindByIdAsync(Id);

            if (user != null)
            {
                return Ok(user);
            }

            return NotFound();

        }

        //[Route("user/{username}")]
        //public async Task<IHttpActionResult> GetUserByName(string username)
        //{
        //    var user = await this.AppUserManager.FindByNameAsync(username);

        //    if (user != null)
        //    {
        //        return Ok(this.userService.GetUser(user));
        //    }

        //    return NotFound();
        //}

        [Route("create")]
        public async Task<IHttpActionResult> CreateUser(DriverViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var driver = Mapper.Map<DriverViewModel, Driver>(viewModel);
            //driverService.CreateDriver(driver);
            //driverService.SaveDriver();

            //var user = new ApplicationUser()
            //{
            //    UserName = createUserModel.Username,
            //    Email = createUserModel.Email,
            //    FirstName = createUserModel.FirstName,
            //    LastName = createUserModel.LastName,
            //    Level = 3,
            //    JoinDate = DateTime.Now.Date,
            //};

            IdentityResult addUserResult = await this.AppUserManager.CreateAsync(driver, viewModel.Password);

            if (!addUserResult.Succeeded)
            {
                return GetErrorResult(addUserResult);
            }

           // Uri locationHeader = new Uri(Url.Link("GetUserById", new { id = driver.Id }));
            //DriverService.CreateDriver(driver);
            //DriverService.SaveDriver();
            //return Ok();
           // userService.CreateUser(driver);
            //userService.SaveUser();
            return Ok();
        }

    }
}
    
