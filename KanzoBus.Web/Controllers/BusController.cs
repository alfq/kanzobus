﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using KanzoBus.Model;
using KanzoBus.Service;
using KanzoBus.Web.ViewModels;

namespace KanzoBus.Web.Controllers
{
    public class BusController : Controller
    {
        private readonly IBusService busService;

        public BusController(IBusService busService)
        {
            this.busService = busService;
        }

        // GET: Bus
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(BusViewModel viewModel)
        {
            if (viewModel != null)
            {
                var bus = Mapper.Map<BusViewModel,Bus>(viewModel);
                busService.CreateBus(bus);
                busService.Save();
                return RedirectToAction("Index", "Home");
            }
            return Content("Error");
        }
    }
}