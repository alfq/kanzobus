﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using KanzoBus.Model;
using KanzoBus.Service;
using KanzoBus.Web.ViewModels;

namespace KanzoBus.Web.Controllers
{
    public class DriverController : Controller
    {
        private readonly IDriverService driverService;

        public DriverController(IDriverService driverService)
        {
            this.driverService = driverService;
        }

        // GET: Driver
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult GetDrivers()
        {
            var drivers = driverService.GetDrivers().ToList();
            return View(drivers);
        }

        [HttpPost]
        public ActionResult Create(DriverViewModel viewModel)
        {
            if (viewModel != null)
            {
                var driver = Mapper.Map<DriverViewModel, Driver>(viewModel);
                driverService.CreateDriver(driver);
                driverService.SaveDriver();
                return RedirectToAction("Index","Home");
            }
            return Content("Error");
        }
    }
}