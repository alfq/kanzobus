﻿using System.Web.Optimization;

namespace KanzoBus.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Content/Bundle/Js")
                .Include("~/Content/js/jquery.min.js",
                "~/Content/js/jquery.easing.1.3.js",
                "~/Content/js/bootstrap.min.js",
                "~/Content/js/bootstrap-datepicker.min.js",
                "~/Content/js/jquery.waypoints.min.js",
                "~/Content/js/sticky.js",
                "~/Content/js/jquery.stellar.min.js",
                "~/Content/js/jquery-2.1.4.min.js",
                "~/Content/js/jquery.countTo.js",
                "~/Content/js/jquery.flexslider-min.js",
                "~/Content/js/hoverIntent.js",
                "~/Content/js/superfish.js",
                "~/Content/js/jquery.magnific-popup.min.js",
                "~/Content/js/magnific-popup-options.js",
                "~/Content/js/bootstrap-datepicker.min.js",
                "~/Content/js/classie.js",
                "~/Content/js/selectFx.js",
                "~/Content/js/modernizr-2.6.2.min.js",
                "~/Content/js/owl.carousel.min.js",
                "~/Content/js/respond.min.js",
                "~/Content/js/main.js",
                "~/Content/js/custom.js"
                //"~/Content/js/google_map.js"
                ));

            bundles.Add(new StyleBundle("~/Content/Bundle/Css")
                .Include("~/Content/css/animate.css", 
                "~/Content/css/bootstrap.css.map",
                "~/Content/css/flaticon.css",
                "~/Content/css/flexslider.css",
                "~/Content/css/icomoon.css",
                "~/Content/css/bootstrap.css",
                "~/Content/css/superfish.css",
                "~/Content/css/magnific-popup.css",
                "~/Content/css/bootstrap-datepicker.min.css",
                "~/Content/css/cs-select.css",
                "~/Content/css/cs-skin-border.css",
                "~/Content/css/owl.carousel.css",
                "~/Content/css/owl.theme.default.min.css",
                "~/Content/css/style.css",
                "~/Content/css/style.css.map",               
                "~/Content/css/themify-icons.css"
                ));

            BundleTable.EnableOptimizations = true;
        }
    }
}