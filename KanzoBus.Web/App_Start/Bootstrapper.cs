﻿using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using KanzoBus.Data.Infrastructure;
using KanzoBus.Data.Repository;
using KanzoBus.Service;
using KanzoBus.Web.Mappings;

namespace KanzoBus.Web
{
    public class Bootstrapper
    {
        public static void Run()
        {
            SetAutoFacContainer();
            AutomapperConfiguration.Configure();
        }

        private static void SetAutoFacContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterType<DbFactory>().As<IDbFactory>().InstancePerRequest();


            //Repositories
            builder.RegisterAssemblyTypes(typeof(DriverRepository).Assembly)
                .Where(d=>d.Name.EndsWith("Repository"))
                .AsImplementedInterfaces().InstancePerRequest();

            
            //Services
            builder.RegisterAssemblyTypes(typeof(DriverService).Assembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerRequest();

            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}