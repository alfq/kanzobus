﻿using System.Collections.Generic;
using KanzoBus.Data.Infrastructure;
using KanzoBus.Data.Repository;
using KanzoBus.Model;

namespace KanzoBus.Service
{
    public interface IUserService
    {
        IEnumerable<User> GetUsers();
        User GetUser(int id);
        void CreateUser(User user);
        void SaveUser();
        User GetUser(string id);
    }


    public class UserService : IUserService
    {
        private readonly IUserRepository _usersRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUserRepository usersRepository, IUnitOfWork unitOfWork)
        {
            _usersRepository = usersRepository;
            _unitOfWork = unitOfWork;
        }

        public UserService()
        {
        }

        public IEnumerable<User> GetUsers()
        {
            var users = _usersRepository.GetAll();
            return users;
        }


        public User GetUser(int id)
        {
            var user = _usersRepository.GetById(id);
            return user;
        }

        public void CreateUser(User user)
        {
            _usersRepository.Add(user);
        }

        public void SaveUser()
        {
            _unitOfWork.Commit();
        }

        public User GetUser(string id)
        {
            var user = _usersRepository.GetById(id);
            return user;
        }
    }

}