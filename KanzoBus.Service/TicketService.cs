﻿using System.Collections.Generic;
using KanzoBus.Data.Infrastructure;
using KanzoBus.Data.Repository;
using KanzoBus.Model;

namespace KanzoBus.Service
{
    public interface ITicketService
    {
        IEnumerable<Ticket> GetAll();
        Ticket GetTicket(int id);
        void CreateTicket(Ticket ticket);
        void RemoveTicket(Ticket ticket);
        void UpdateTicket(Ticket ticket);
        void Save();
    }


    public class TicketService : ITicketService
    {
        private readonly ITicketRepository _ticketRepository;
        private readonly IUnitOfWork _unitOfWork;

        public TicketService(ITicketRepository ticketRepository, IUnitOfWork unitOfWork)
        {
            _ticketRepository = ticketRepository;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Ticket> GetAll()
        {
            return _ticketRepository.GetAll();
        }

        public Ticket GetTicket(int id)
        {
            return _ticketRepository.GetById(id);
        }

        public void CreateTicket(Ticket ticket)
        {
            _ticketRepository.Add(ticket);
        }

        public void RemoveTicket(Ticket ticket)
        {
            _ticketRepository.Delete(ticket);
        }

        public void UpdateTicket(Ticket ticket)
        {
            _ticketRepository.Update(ticket);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }
    }
}