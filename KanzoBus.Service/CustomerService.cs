﻿using System;
using System.Collections.Generic;
using System.Linq;
using KanzoBus.Data.Infrastructure;
using KanzoBus.Data.Repository;
using KanzoBus.Model;

namespace KanzoBus.Service
{
    public interface ICustomerService
    {
        IEnumerable<Customer> GetCustomers();
        Customer GetCustomer(int id);
        void CreateCustomer(Customer customer);
        void SaveCustomer();
    }

    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository customerRepository;
        private readonly IUnitOfWork unitOfWork;

        public CustomerService(ICustomerRepository customerRepository, IUnitOfWork unitOfWork)
        {
            this.customerRepository = customerRepository;
            this.unitOfWork = unitOfWork;
        }

        public void CreateCustomer(Customer customer)
        {
            customerRepository.Add(customer);
        }

        public Customer GetCustomer(int id)
        {
            var customer = customerRepository.GetById(id);
            return customer;
        }

        public IEnumerable<Customer> GetCustomers()
        {
            var customers = customerRepository.GetAll().ToList();
            return customers;
        }

        public void SaveCustomer()
        {
            unitOfWork.Commit();
        }
    }
}