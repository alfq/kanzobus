using System.Collections.Generic;
using KanzoBus.Data.Infrastructure;
using KanzoBus.Data.Repository;
using KanzoBus.Model;

namespace KanzoBus.Service
{
    public interface IBookingService
    {
        IEnumerable<Booking> GetAll();
        Booking GetBooking(int id);
        void CreateBooking(Booking booking);
        void Save();
        void RemoveBooking(int id);
        void UpdateBooking(Booking booking);
    }

    public class BookingService : IBookingService
    {
        private readonly IBookingRepository _bookingRepository;
        private readonly UnitOfWork _unitOfWork;

        public BookingService(IBookingRepository bookingRepository, UnitOfWork unitOfWork)
        {
            _bookingRepository = bookingRepository;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Booking> GetAll()
        {
            return _bookingRepository.GetAll();
        }

        public Booking GetBooking(int id)
        {
            var booking = _bookingRepository.GetById(id);
            return booking;
        }

        public void CreateBooking(Booking booking)
        {
            _bookingRepository.Add(booking);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void RemoveBooking(int id)
        {
            _bookingRepository.Delete(b=>b.BookingId == id);
        }

        public void UpdateBooking(Booking booking)
        {
            _bookingRepository.Update(booking);
        }
    }
}