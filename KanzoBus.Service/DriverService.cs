﻿using System.Collections.Generic;
using KanzoBus.Data.Infrastructure;
using KanzoBus.Data.Repository;
using KanzoBus.Model;

namespace KanzoBus.Service
{
    public interface IDriverService
    {
        IEnumerable<Driver> GetDrivers();
        IEnumerable<Driver> GetQualificationTypeDrivers(string qualificationType);
        Driver GetDriver(int id);
        void CreateDriver(Driver driver);
        void SaveDriver();
    }


    public class DriverService : IDriverService
    {
        private readonly IDriverRepository _driversRepository;
        private readonly IUnitOfWork _unitOfWork;

        public DriverService(IDriverRepository driversRepository, IUnitOfWork unitOfWork)
        {
            _driversRepository = driversRepository;
            _unitOfWork = unitOfWork;
        }

        public DriverService()
        {
        }

        public IEnumerable<Driver> GetDrivers()
        {
            var drivers = _driversRepository.GetAll();
            return drivers;
        }

        public IEnumerable<Driver> GetQualificationTypeDrivers(string qualificationType)
        {
            var driverByQualification = _driversRepository.GetMany(d=>d.Qualification == qualificationType);
            return driverByQualification;
        }

        public Driver GetDriver(int id)
        {
            var driver = _driversRepository.GetById(id);
            return driver;
        }

        public void CreateDriver(Driver driver)
        {
            _driversRepository.Add(driver);
        }

        public void SaveDriver()
        {
            _unitOfWork.Commit();
        }
    }

}
