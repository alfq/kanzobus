﻿using System.Collections.Generic;
using KanzoBus.Data.Infrastructure;
using KanzoBus.Data.Repository;
using KanzoBus.Model;

namespace KanzoBus.Service
{
    public interface IBusService
    {
        IEnumerable<Bus> GetBuses();
        Bus GetBus(int id);
        void CreateBus(Bus bus);
        void Save();
    }

    public class BusService : IBusService
    {
        private readonly IBusRepository _busRepository;
        private readonly IUnitOfWork _unitOfWork;

        public BusService(IBusRepository busRepository, IUnitOfWork unitOfWork)
        {
            _busRepository = busRepository;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Bus> GetBuses()
        {
            return _busRepository.GetAll();
        }

        public Bus GetBus(int id)
        {
           var bus = _busRepository.GetById(id);
            return bus;
        }

        public void CreateBus(Bus bus)
        {
            _busRepository.Add(bus);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }
    }
}