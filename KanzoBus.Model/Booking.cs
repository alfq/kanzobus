﻿using System;

namespace KanzoBus.Model
{
    public class Booking
    {
        public int BookingId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        public BusType BusType { get; set; }
        public string BoadingLocation { get; set; }

    }
}