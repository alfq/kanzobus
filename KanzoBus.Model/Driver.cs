﻿using System.ComponentModel.DataAnnotations.Schema;
using KanzoBus.Model.General;

namespace KanzoBus.Model
{
    public class Driver : User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DriverId { get; set; }
        //public string FirstName { get; set; }
        //public string LastName { get; set; }
        //public string FullName => LastName + " " + FirstName;
        //public string PasswordHash { get; private set; }
        //public string Email { get; set; }

        //public string Password
        //{
        //    get { return PasswordHash; }
        //    set {PasswordHash = Encryptor.Encrypt(value); }
        //}

        public string Qualification { get; set; }

        //public int BusId { get; set; }
        //public Bus Bus { get; set; }
    }
}
