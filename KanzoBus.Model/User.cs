using Microsoft.AspNet.Identity.EntityFramework;

namespace KanzoBus.Model
{
    public class User : IdentityUser
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        //public int? DriverId { get; set; }
        //public Driver Driver { get; set; }

        //public int CustomerId { get; set; }
        //public Customer Customer { get; set; }
    }
}