﻿using System;

namespace KanzoBus.Model
{
    public class Ticket
    {
        public int TicketId { get; set; }

        public int BookingId { get; set; }
        public Booking Booking { get; set; }

        public int? DriverId { get; set; }
        public Driver Driver { get; set; }

        public int CustomerId { get; set; }
        public Customer Customer { get; set; }

        public DateTime DateTime { get; set; }

    }
}