using System.Collections.Generic;

namespace KanzoBus.Model
{
    public class Bus
    {
        public int BusId { get; set; }
        public string BusNumber { get; set; }
        public BusType ServiceType { get; set; }
        public int SeatsNumber { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public List<User> Users { get; set; }

    }

    public enum BusType
    {
        Express = 1,
        Luxury =2,
        Economy = 3
    }

}