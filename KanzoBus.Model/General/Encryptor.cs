﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace KanzoBus.Model.General
{
    public static class Encryptor
    {
        public static string Encrypt(string password)
        {
            if (string.IsNullOrWhiteSpace(password)) throw new NullReferenceException();
            MD5 md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(Encoding.ASCII.GetBytes(password));

            var result = md5.Hash;
            var strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                strBuilder.Append(result[i].ToString("x2"));
            }
            return strBuilder.ToString();
        }

    }
}